# Monitoring

This Application does not do much, it just has a rolebinding for the
`openshift-monitoring` for your gitops SA. This is because upstream uses a
single in a ConfigMap, filled with YAML, to configure the monitoring stack.
What this means for us is that is almost impossibly hard to do a nice example
config, for which you then use overlays to configure, since you can override _parts_  of a ConfigMap key easily.

What we suggest instead is that you just add your own
`cluster-monitoring-config` and `user-workload-monitoring-config` ConfigMaps as overlays.
