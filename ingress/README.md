# Ingress

To configure IngressControllers use the examples in the `assets` directory as a
base, and configure your own IngressControllers as resources.

The two rolebindings allow ArgoCd to create the necessary objects in the
`openshift-ingress` and `openshift-ingress-operator` namespaces.

The example has single IngressController with a custom certificate secret, and
a templated secret. Remeber to change the `defaultCertificate` `replicas` and
`nodeSelector` fields as necessary.

Check [the documentation for your version of OpenShift here](https://docs.openshift.com/container-platform/4.9/networking/ingress-operator.html)
