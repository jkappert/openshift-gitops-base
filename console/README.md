# Console

This Application configures the console. The abse as defined in this repo does
not add any extra configurations, but you can add things like
ConsoleNotification, ConsoleLinks manifests in an overlay, or even change the
logo with a strategicMergePatch and a configMap.

[Check the upstream documentation for your version of
OpenShift](https://docs.openshift.com/container-platform/4.9/web_console/customizing-the-web-console.html)
for all customization possibilities.
