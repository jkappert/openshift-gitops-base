# OpenShift Cluster Storage / OpenShift Data Foundation

This Application install the OpenShift Storage Operator, but does not yet configure it.

Succesful use will require the use of the `local_storage` component as well, to
set up LocalVolumes for the disks on your storage nodes, and assign them to a
`StorageClass`.

# Operator Version

You may want to patch the version of the operator being installed, you can do this with `patchesSstrategicMerge` if you so desire:

```
patchesStrategicMerge:
- |
  apiVersion: operators.coreos.com/v1alpha1
  kind: Subscription
  metadata:
    name: ocs-operator
  spec:
    channel: newversion
```

# Examples

An example for a StorageCluster is included in the samples directory


# Warning

The operator does not always clean everything it needs to, if you face mons not
starting nuke everything, clear the `/var/lib/rook` directory on your storage
nodes, and try again.
