# oauth

OpenShift configures authentication providers in an object called OAUth/cluster.

This object can configure multiple different types of authentication
providers. For the docs see the [relevant documentation for your cluster
version](https://docs.openshift.com/container-platform/4.9/post_installation_configuration/preparing-for-users.html).


## Warning
This repo should **not** be used as is, but serves as an example of _some of
the things_  that you can configure

## A note on usage with ArgoCd

When using a custom Certificate Authority (CA), the OpenShift `oauth` operator
copies your ConfigMap holding the custom CA from the `openshift-config`
namespace to a new name in the `openshift-authentication` namespace. Since any
labels assigned to it (including the `app.kubernetes.io/instance` label) are
copied as well, ArgoCD now thinks it is managing the copied ConfigMap, and will
try to delete it, now the authentication operator copies it again, and we get
into an endless loop of copying and deleting.

To combat this add the following **annotation** to you CA ConfigMap:
```
annotations:
  argocd.argoproj.io/compare-options: IgnoreExtraneous
```
