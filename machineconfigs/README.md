# machineconfigs

This Application should **not** be applied directly to your cluster. Instead, use the examples in the `assets` directory to create machineConfigs for *your* cluster. 

# Examples

The included examples add a 'chrony.conf' to both the `master` and `worker` machineConfigPool (and any pools that derive from them) using the {0..4}.pool.ntp.org timeservers.

The chrony.conf embedded in them looks like this when decoded from base64:

```
server 0.pool.ntp.org iburst
server 1.pool.ntp.org iburst
server 2.pool.ntp.org iburst
server 3.pool.ntp.org iburst
driftfile /var/lib/chrony/drift
makestep 1.0 3
rtcsync
logdir /var/log/chrony
```
